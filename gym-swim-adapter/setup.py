from setuptools import setup

setup(name='gym_swim_adapter',
      version='0.0.1',
      install_requires=['gym', 'numpy', 'docker']
)