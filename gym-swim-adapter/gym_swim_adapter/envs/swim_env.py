import atexit
import json
import pathlib
import random
import socket
import sys
import time
from abc import ABC
from collections import deque
from datetime import datetime
from typing import Literal, Union

import docker
import gym
import numpy as np
from gym import spaces


class SwimBaseEnv(gym.Env, ABC):
    ALL_OBSERVATION_VARIABLES = {
        'basic_response_time': (0.0, np.inf),
        'opt_response_time': (0.0, np.inf),
        'basic_throughput': (0.0, np.inf),
        'opt_throughput': (0.0, np.inf),
        'avg_response_time': (0.0, np.inf),
        'utilization': (0.0, np.inf),
        'current_dimmer': (0.0, 1.0),
        'active_servers': (0.0, 3),
        'max_servers': (0.0, 3),
        'is_booting': (0.0, 1.0),
        'boot_remaining': (0.0, np.inf),
        'request_arrival_mean': (0.0, np.inf),
        'request_arrival_moving_mean': (0.0, np.inf),  # markov property
        'request_arrival_variance': (0.0, np.inf),
        'request_arrival_moving_variance': (0.0, np.inf),  # markov property
        'low_fidelity_service_time': (0.0, np.inf),
        'low_fidelity_service_time_variance': (0.0, np.inf),
        'service_time': (0.0, np.inf),
        'service_time_variance': (0.0, np.inf),
        'server_threads': (0.0, 100),
    }
    NUMBER_OF_DISTINCT_RUNS = 31  # 88 + 31  # world cup + edgar
    REWARD_TYPES = Literal['utility', 'aggregated_reward', 'decomposed_reward']

    class SocketClosed(Exception):
        pass

    def __init__(
            self,
            observation_semantics = tuple(ALL_OBSERVATION_VARIABLES.keys()),
            reward_type: REWARD_TYPES = 'aggregated_reward',
            user_satisfaction_reward_weight: float = 1.0,
            revenue_reward_weight: float = 1.0,
            illegal_action_punishment: float = -0.02,
            any_action_punishment: float = -0.02,
            least_satisfactory_response_time_threshold: float = 1.0,
            most_satisfactory_response_time_threshold: float = 0.02,
            revenue_request_optional_content_factor: float = 3.0,
            docker_image_name='swim',
            traces_dir: str = pathlib.Path(__file__).parent.parent.parent.parent / pathlib.Path('swim-docker', 'traces'),
            store_simulation_results=True,
            simulation_results_dir='simulation_results',
            docker_host='localhost',  # change when using docker-compose
            seed=random.randrange(sys.maxsize),
            round_robin_trace_selection=False,  # set True for evaluation
            verbose=False,
            moving_average_past_steps=20,  # 10s per step -> default: 200s interval
            container_name='auto_generated_swim_simulation_removed_after_run',
            exposed_port=60396) -> None:
        super().__init__()

        self.traces_dir = traces_dir
        self.observation_semantics = observation_semantics
        self._reward_type = reward_type
        self._user_satisfaction_reward_weight = user_satisfaction_reward_weight
        self._revenue_reward_weight = revenue_reward_weight
        self._illegal_action_punishment = illegal_action_punishment
        self._any_action_punishment = any_action_punishment
        self._least_satisfactory_response_time_threshold = \
            least_satisfactory_response_time_threshold
        self._most_satisfactory_response_time_threshold = \
            most_satisfactory_response_time_threshold
        self._revenue_request_optional_content_factor = revenue_request_optional_content_factor
        self._docker_image_name = docker_image_name
        self._store_simulation_results = store_simulation_results
        self._results_path = pathlib.Path().resolve() / simulation_results_dir
        self._docker_host = docker_host
        self._round_robin_trace_selection = round_robin_trace_selection
        self._verbose = verbose
        self._moving_average_past_steps = moving_average_past_steps
        self._container_name = container_name
        self._exposed_port = exposed_port

        self._make_rng(seed)
        self._construct_observation_space()
        self._setup_docker()

        self.current_run_name = None
        self._last_raw_observation: dict = None
        self._current_action: int = None
    
    def get_config(self) -> dict:
        return {
            'reward_type': self._reward_type,
            'user_satisfaction_reward_weight': self._user_satisfaction_reward_weight,
            'revenue_reward_weight': self._revenue_reward_weight,
            'illegal_action_punishment': self._illegal_action_punishment,
            'any_action_punishment': self._any_action_punishment,
            'moving_average_past_steps': self._moving_average_past_steps,
            'least_satisfactory_response_time_threshold': self._least_satisfactory_response_time_threshold,
            'most_satisfactory_response_time_threshold': self._most_satisfactory_response_time_threshold,
            'revenue_request_optional_content_factor': self._revenue_request_optional_content_factor,
        }

    def get_reward_channel_semantics(self) -> tuple[str, ...]:
        if self._reward_type == 'decomposed_reward':
            return 'Running Costs', 'User Satisfaction', 'Revenue'
        else:
            return self._reward_type,

    def _initialize_moving_averages(self):
        self._request_arrival_moving_mean = \
            deque(maxlen=self._moving_average_past_steps)
        self._request_arrival_moving_variance = \
            deque(maxlen=self._moving_average_past_steps)

    def _update_moving_averages(self, raw_observation):
        self._request_arrival_moving_mean \
            .append(raw_observation['request_arrival_mean'])
        self._request_arrival_moving_variance \
            .append(raw_observation['request_arrival_variance'])

    def _get_request_arrival_moving_variance(self):
        return np.mean(self._request_arrival_moving_variance)

    def _get_request_arrival_moving_mean(self):
        return np.mean(self._request_arrival_moving_mean)

    def _add_moving_averages_to_raw_observation(self, raw_observation):
        self._update_moving_averages(raw_observation)
        return {
            **raw_observation,
            'request_arrival_moving_mean':
                self._get_request_arrival_moving_mean(),
            'request_arrival_moving_variance':
                self._get_request_arrival_moving_variance(),
        }

    def _prepare_new_run(self):
        name_format = f'%Y_%m_%d_%H_%M_%S_seed_{self._current_seed}'
        self.current_run_name = datetime.now().strftime(name_format)
        self._results_path /= self.current_run_name
        self.__num_of_runs = 0

        if self._store_simulation_results:
            self._make_results_base_dir()

        self._initialize_moving_averages()

    def _setup_docker(self):
        self.docker_client = docker.from_env()
        self.docker_client.images.get(self._docker_image_name)  # verify exists
        self.container = None
        atexit.register(self._cleanup)  # remove container after run

    def _make_rng(self, seed):
        self._current_seed = seed
        self._rng = random.Random(self._current_seed)

    def _construct_observation_space(self):
        lows = [self.ALL_OBSERVATION_VARIABLES[k][0]
                for k in self.observation_semantics]
        highs = [self.ALL_OBSERVATION_VARIABLES[k][1]
                 for k in self.observation_semantics]
        self.observation_space = spaces.Box(
            low=np.array(lows),
            high=np.array(highs),
            dtype=np.float64,
        )

    def _execute_action(self):
        message = self._convert_action_to_message(self._current_action)
        self._send_message_to_swim(message)

    def _convert_action_to_message(self, action: int) -> dict:
        raise NotImplementedError

    def _close_socket_if_exists(self):
        try:
            self.socket.close()
        except AttributeError:
            pass

    def _cleanup(self):
        self._close_socket_if_exists()
        self._remove_swim_container_if_exists()

    def step(self, action) -> tuple[np.ndarray, Union[float, tuple[float]], bool, dict]:
        self._current_action = action
        self._execute_action()
        observation, reward = self._get_next_observation_reward_retry()
        return observation, reward, False, {'current_trace_id': self._trace_id}

    def _get_next_observation_reward_retry(self, max_trials=10):
        current_trial = 0
        while current_trial <= max_trials - 1:
            time.sleep(current_trial * 5)
            current_trial += 1

            try:
                return self._get_next_observation_reward()
            except self.SocketClosed:
                self.socket.close()
                self._start_and_connect_container()

        raise self.SocketClosed(f'Socket closed. Couldn\'t reopen '
                                'socket after {max_trials} trials.')

    def _send_nop_action(self):
        self._send_message_to_swim({})

    def reset(self) -> np.ndarray:
        self._prepare_new_run()
        self._start_and_connect_container()
        observation = self._get_next_observation_reward_retry()[0]
        return observation

    def seed(self, new_seed: int) -> list[int]:
        if new_seed is not None:
            self._current_seed = new_seed
            self._rng = random.Random(new_seed)

        return [self._current_seed]

    def _send_message_to_swim(self, message: dict):
        self.socket.sendall(json.dumps(message).encode('utf-8'))

    def _get_next_observation_reward(self) -> tuple[np.ndarray, Union[float, tuple[float]]]:
        next_message = self._receive_next_message()
        raw_observation = next_message['observation']
        prepared_observation = self._construct_observation(raw_observation)
        utility = next_message['utility']
        reward = self.calculate_reward(utility, raw_observation)
        self._last_raw_observation = raw_observation
        return prepared_observation, reward

    def _construct_observation(self, raw_observation: dict) -> np.ndarray:
        raw_observation = \
            self._add_moving_averages_to_raw_observation(raw_observation)
        observation = [raw_observation[k] for k in self.observation_semantics]
        return np.array(observation)

    def calculate_reward(
            self,
            utility: float,
            observation: dict) -> Union[float, tuple[float]]:
        if self._reward_type == 'utility':
            return utility
        elif self._reward_type == 'aggregated_reward':
            return sum(self._calculate_decomposed_reward(observation))
        elif self._reward_type == 'decomposed_reward':
            return self._calculate_decomposed_reward(observation)
        else:
            raise RuntimeError('Invalid reward type.')

    def _last_was_no_operation(self):
        raise NotImplementedError('Must be implemented by inheriting class.')

    def _last_was_illegal_action(self):
        raise NotImplementedError('Must be implemented by inheriting class.')

    def _calculate_aggregated_reward(self, observation: dict):
        return sum(self._calculate_decomposed_reward(observation))

    def _calculate_decomposed_reward(self, observation: dict) -> tuple[float, float, float]:
        energy_cost = \
            self._calculate_energy_cost(observation['active_servers'])
        user_satisfaction = \
            self._calculate_user_satisfaction(observation['avg_response_time'])
        revenue = \
            self._calculate_revenue(observation['opt_throughput'])

        illegal_action_penalty = self._last_was_illegal_action() * self._illegal_action_punishment
        any_action_penalty = (1 - self._last_was_no_operation()) * self._any_action_punishment
        penalty = illegal_action_penalty + any_action_penalty
        return (
            energy_cost + penalty,
            self._user_satisfaction_reward_weight * user_satisfaction + penalty,
            self._revenue_reward_weight * revenue + penalty,
        )

    @staticmethod
    def _calculate_energy_cost(num_active_servers: int):
        cost_per_server_per_interval = 0.05
        return num_active_servers * -cost_per_server_per_interval

    def _calculate_user_satisfaction(self, avg_response_time: float):
        max_user_satisfaction_reward = 0.5
        if avg_response_time <= self._most_satisfactory_response_time_threshold:
            raw_satisfaction = max_user_satisfaction_reward
        elif avg_response_time >= self._least_satisfactory_response_time_threshold:
            raw_satisfaction = -max_user_satisfaction_reward - (avg_response_time - self._least_satisfactory_response_time_threshold) / 20
        else:
            # linearly extrapolate between -max_user_satisfaction_reward and
            # max_user_satisfaction_reward
            interval_size = self._least_satisfactory_response_time_threshold - self._most_satisfactory_response_time_threshold
            normalized_response_time = avg_response_time - self._most_satisfactory_response_time_threshold
            raw_satisfaction = max_user_satisfaction_reward - (
                        2 * max_user_satisfaction_reward * normalized_response_time / interval_size)
        
        return raw_satisfaction

    def _calculate_revenue(self, optional_content_throughput: float):
        # divide by 1000 to keep the reward stream in balance with the other ones
        return optional_content_throughput * self._revenue_request_optional_content_factor / 1000

    def _start_and_connect_container(self):
        if self._round_robin_trace_selection:
            self._trace_id = \
                self.__num_of_runs % self.NUMBER_OF_DISTINCT_RUNS
        else:
            self._trace_id = \
                self._rng.randrange(self.NUMBER_OF_DISTINCT_RUNS)

        self.__num_of_runs += 1
        self._start_swim_docker()
        self._connect_when_connection_open()

    def _receive_next_message(self, message_delimiter='|'):
        message = ''
        while message_delimiter not in message:
            data = self.socket.recv(1024)
            if not data:
                raise self.SocketClosed
            message += data.decode()

        message = message.rstrip()[:-1]  # remove message delimiter
        return json.loads(message)

    def _connect_when_connection_open(self):
        while True:
            time.sleep(.5)
            try:
                self._create_and_connect_socket()
                return
            except ConnectionRefusedError:
                self.socket.close()
                self.socket = None

    def _create_and_connect_socket(self):
        self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.socket.connect((self._docker_host, self._exposed_port))

    def _make_results_base_dir(self):
        self._results_path.mkdir(parents=True, exist_ok=True)

    def _get_current_run_results_dir(self, trace):
        current_run_dir = \
            self._results_path / f'{self.__num_of_runs:06}_trace_{trace}'
        current_run_dir.mkdir(parents=True, exist_ok=True)
        return current_run_dir

    def _remove_swim_container_if_exists(self):
        try:
            container = self.docker_client.containers.get(self._container_name)
            container.remove(force=True)
        except docker.errors.NotFound:
            pass  # no container existent

        self.container = None

    def _start_swim_docker(self):
        self._remove_swim_container_if_exists()
        if self._verbose:
            print(f'Starting simulation using trace {self._trace_id}')

        volumes = {
            self.traces_dir: {
                'bind': '/root/seams-swim/swim/simulations/swim/traces',
                'mode': 'ro'
            },
        }

        if self._store_simulation_results:
            volumes[self._get_current_run_results_dir(self._trace_id)] = {
                'bind': '/root/seams-swim/results',
                'mode': 'rw'
            }

        self.container = self.docker_client.containers.run(
            image=self._docker_image_name,
            command=f'{self._trace_id} {self.NUMBER_OF_DISTINCT_RUNS}',
            remove=False,
            detach=True,
            name=self._container_name,
            ports={'60396/tcp': self._exposed_port},
            volumes=volumes
        )

    def render(self, mode='human'):
        print(f'Current trace: {self._trace_id}')


class SwimDiscreteActionsEnv(SwimBaseEnv):
    action_space = spaces.Discrete(5)
    action_semantics: tuple[str, ...] = (
            'No Operation',
            'Increase Dimmer',
            'Decrease Dimmer',
            'Add Server',
            'Remove Server',
    )

    def _last_was_no_operation(self):
        return self._current_action == 0

    def _last_was_illegal_action(self) -> bool:
        return self.is_useless_action(
            self._last_raw_observation,
            self._current_action
        ) if self._current_action is not None else False

    @staticmethod
    def is_useless_action_raw_state(state: np.ndarray, action: int) -> bool:
        observation_semantics = \
            list(SwimBaseEnv.ALL_OBSERVATION_VARIABLES.keys())
        return SwimDiscreteActionsEnv.is_useless_action(
            {ose: state[i] for i, ose in enumerate(observation_semantics)},
            action
        )

    @staticmethod
    def is_useless_action(observation: dict, action: int) -> bool:
        current_dimmer = observation['current_dimmer']
        useless_dimmer_increase = action == 1 and current_dimmer == 1.0
        useless_dimmer_decrease = action == 2 and current_dimmer == 0.0

        active_servers = observation['active_servers']
        is_booting = observation['is_booting']
        max_servers = observation['max_servers']
        useless_server_add = \
            action == 3 and active_servers + is_booting >= max_servers
        useless_server_remove = \
            action == 4 and active_servers <= 1

        return any((
            useless_dimmer_increase,
            useless_dimmer_decrease,
            useless_server_add,
            useless_server_remove
        ))

    def _convert_action_to_message(self, action: int) -> dict:
        if action == 0:
            return {}  # NOP
        elif action == 1:
            return {'dimmer_offset': 0.05}
        elif action == 2:
            return {'dimmer_offset': -0.05}
        elif action == 3:
            return {'add_server': True}
        elif action == 4:
            return {'remove_server': True}
        else:
            raise RuntimeError(f'Action {action} out of range.')


if __name__ == '__main__':  # testing script (random agent)
    sga = SwimDiscreteActionsEnv(
        reward_type='decomposed_reward',
        store_simulation_results=False,
    )
    sga.reset()
    while True:
        sga.step(sga.action_space.sample())
