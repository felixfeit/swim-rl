from gym.envs.registration import register

register(
    id='swim-discrete-actions-v0',
    entry_point='gym_foo.envs:SwimDiscreteActionsEnv',
)
