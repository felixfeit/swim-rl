#!/bin/bash
if [ $1 = "random" ]; then
    ./run_random_forever.sh $2  # $2 is number of traces
else
    ./run.sh ReactiveSocket $(($1 % $2))  # $2 is number of traces
fi