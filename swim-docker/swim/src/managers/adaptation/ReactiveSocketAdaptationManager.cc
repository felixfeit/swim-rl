#include "ReactiveSocketAdaptationManager.h"
#include "managers/adaptation/UtilityScorer.h"
#include "managers/execution/AllTactics.h"
#include "json.hpp"
#include <ctime>
#include <iostream>
#include <string>
#include <boost/asio.hpp>

using boost::asio::ip::tcp;
using namespace std;
using json = nlohmann::json;


Define_Module(ReactiveSocketAdaptationManager);

void ReactiveSocketAdaptationManager::finish() {
    sock.shutdown(boost::asio::ip::tcp::socket::shutdown_both);
    sock.close();
}

void ReactiveSocketAdaptationManager::initialize(int stage) {
    BaseAdaptationManager::initialize(stage);

    if (stage == 0)
        acceptor.accept(sock);
}

Tactic* ReactiveSocketAdaptationManager::evaluate() {
    Model* pModel = getModel();

    // send observation to client
    json observationJson = {
        {"observation", {
            {"basic_response_time", pModel->getObservations().basicResponseTime},
            {"opt_response_time", pModel->getObservations().optResponseTime},
            {"basic_throughput", pModel->getObservations().basicThroughput},
            {"opt_throughput", pModel->getObservations().optThroughput},
            {"avg_response_time", pModel->getObservations().avgResponseTime},
            {"utilization", pModel->getObservations().utilization},
            {"current_dimmer", pModel->getDimmerFactor()},
            {"active_servers", pModel->getConfiguration().getActiveServers()},
            {"max_servers", pModel->getMaxServers()},
            {"is_booting", pModel->getServers() > pModel->getActiveServers()},
            {"boot_remaining", pModel->getConfiguration().getBootRemain()},
            {"request_arrival_mean", pModel->getEnvironment().getArrivalMean()},
            {"request_arrival_variance", pModel->getEnvironment().getArrivalVariance()},

            // TODO: check if the following stay constant (and remove if its the case)
            {"low_fidelity_service_time", pModel->getLowFidelityServiceTime()},
            {"low_fidelity_service_time_variance", pModel->getLowFidelityServiceTimeVariance()},
            {"service_time", pModel->getServiceTime()},
            {"service_time_variance", pModel->getServiceTimeVariance()},
            {"server_threads", pModel->getServerThreads()},
        }},
        {"utility", UtilityScorer::getAccruedUtility(
            *pModel,
            pModel->getConfiguration(),
            pModel->getEnvironment(),
            pModel->getObservations()
        )}
    };

    sock.send(boost::asio::buffer(observationJson.dump() + "|"));

    // wait for action receipt
    char data[1024];
    boost::system::error_code error;
    size_t length = sock.read_some(boost::asio::buffer(data), error);
    if (error)
        throw boost::system::system_error(error);
    data[length] = '\0';

    auto actionJson = json::parse(data);
    bool addServer = actionJson.value("add_server", false);
    bool removeServer = actionJson.value("remove_server", false);
    bool isServerBooting = pModel->getServers() > pModel->getActiveServers();

    MacroTactic* pMacroTactic = new MacroTactic;
    if (addServer != removeServer && !isServerBooting) {
        if (addServer && pModel->getServers() < pModel->getMaxServers())
            pMacroTactic->addTactic(new AddServerTactic);

        if (removeServer && pModel->getServers() > 1)
            pMacroTactic->addTactic(new RemoveServerTactic);
    }

    if (actionJson.contains("dimmer")) {
        double newDimmer = actionJson["dimmer"];
        newDimmer = max(0.0, newDimmer);
        newDimmer = min(1.0, newDimmer);
        pMacroTactic->addTactic(new SetDimmerTactic(newDimmer));
    } else if (actionJson.contains("dimmer_offset")) {
        double dimmerOffset = actionJson["dimmer_offset"];
        double offsetDimmer = pModel->getDimmerFactor() + dimmerOffset;
        offsetDimmer = max(0.0, offsetDimmer);
        offsetDimmer = min(1.0, offsetDimmer);
        pMacroTactic->addTactic(new SetDimmerTactic(offsetDimmer));
    }

    return pMacroTactic;
}
