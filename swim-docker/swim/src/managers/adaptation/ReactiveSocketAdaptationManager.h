#ifndef __PLASASIM_REACTIVESOCKETADAPTATIONMANAGER_H_
#define __PLASASIM_REACTIVESOCKETADAPTATIONMANAGER_H_

#include "BaseAdaptationManager.h"
#include <boost/asio.hpp>

using boost::asio::ip::tcp;

/**
 * Simple reactive socket adaptation manager
 */
class ReactiveSocketAdaptationManager : public BaseAdaptationManager
{
    boost::asio::io_service io_service;
    tcp::acceptor           acceptor;
    tcp::socket             sock;

    public:
        ReactiveSocketAdaptationManager ()
                : acceptor(io_service, tcp::endpoint(tcp::v4(), 60396))
                , sock(io_service) {}

    protected:
        virtual Tactic* evaluate();
        virtual void initialize(int stage);
        virtual void finish();
};

#endif
