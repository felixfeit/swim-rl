# from https://www.sec.gov/dera/data/edgar-log-file-data-set.html
import csv
from datetime import datetime
import urllib.request
import zipfile
from io import TextIOWrapper
from pathlib import Path


def file_exists_already(url):
    url_year = url[-12:-8]
    url_month = url[-8:-6]
    url_day = url[-6:-4]
    files_in_dir = [str(f.stem) for f in Path(__file__).parent.glob('**/*') if f.is_file()]
    return f'{url_year}-{url_month}-{url_day}' in files_in_dir


def download_and_convert_file(url, min_increment=1e-9):
    if file_exists_already(url):
        return

    filehandle, _ = urllib.request.urlretrieve(url)
    zip_file_object = zipfile.ZipFile(filehandle, 'r')
    file_name = [n for n in zip_file_object.namelist() if n.endswith('.csv')][0]
    
    time_increments = []
    day_string = None
    with TextIOWrapper(zip_file_object.open(file_name)) as csvfile:
        csv_reader = csv.reader(csvfile, delimiter=',')
        next(csv_reader)
        first_row = next(csv_reader)
        day_string = first_row[1]
        time_increments.append(min_increment)
        last_time = datetime.strptime(first_row[2], '%H:%M:%S')
        for row in csv_reader:
            new_time = datetime.strptime(row[2], '%H:%M:%S')
            increment = (new_time - last_time).total_seconds()
            time_increments.append(max(increment, min_increment))
            last_time = new_time

    out_file = day_string + '.delta'
    with open(out_file, 'w') as f:
        for increment in time_increments:
           f.write(f'{increment}\n')


def generate_comma_separated_filename_string(prefix='traces/edgar_2017/'):
    file_names = sorted([prefix + str(x.name) for x in list(Path(__file__).parent.glob('*.delta')) if x.is_file()])
    return '"' + '", "'.join(file_names) + '"'


url_list_january_2017 = [
    'http://www.sec.gov/dera/data/Public-EDGAR-log-file-data/2017/Qtr1/log20170101.zip',
    'http://www.sec.gov/dera/data/Public-EDGAR-log-file-data/2017/Qtr1/log20170102.zip',
    'http://www.sec.gov/dera/data/Public-EDGAR-log-file-data/2017/Qtr1/log20170103.zip',
    'http://www.sec.gov/dera/data/Public-EDGAR-log-file-data/2017/Qtr1/log20170104.zip',
    'http://www.sec.gov/dera/data/Public-EDGAR-log-file-data/2017/Qtr1/log20170105.zip',
    'http://www.sec.gov/dera/data/Public-EDGAR-log-file-data/2017/Qtr1/log20170106.zip',
    'http://www.sec.gov/dera/data/Public-EDGAR-log-file-data/2017/Qtr1/log20170107.zip',
    'http://www.sec.gov/dera/data/Public-EDGAR-log-file-data/2017/Qtr1/log20170108.zip',
    'http://www.sec.gov/dera/data/Public-EDGAR-log-file-data/2017/Qtr1/log20170109.zip',
    'http://www.sec.gov/dera/data/Public-EDGAR-log-file-data/2017/Qtr1/log20170110.zip',
    'http://www.sec.gov/dera/data/Public-EDGAR-log-file-data/2017/Qtr1/log20170111.zip',
    'http://www.sec.gov/dera/data/Public-EDGAR-log-file-data/2017/Qtr1/log20170112.zip',
    'http://www.sec.gov/dera/data/Public-EDGAR-log-file-data/2017/Qtr1/log20170113.zip',
    'http://www.sec.gov/dera/data/Public-EDGAR-log-file-data/2017/Qtr1/log20170114.zip',
    'http://www.sec.gov/dera/data/Public-EDGAR-log-file-data/2017/Qtr1/log20170115.zip',
    'http://www.sec.gov/dera/data/Public-EDGAR-log-file-data/2017/Qtr1/log20170116.zip',
    'http://www.sec.gov/dera/data/Public-EDGAR-log-file-data/2017/Qtr1/log20170117.zip',
    'http://www.sec.gov/dera/data/Public-EDGAR-log-file-data/2017/Qtr1/log20170118.zip',
    'http://www.sec.gov/dera/data/Public-EDGAR-log-file-data/2017/Qtr1/log20170119.zip',
    'http://www.sec.gov/dera/data/Public-EDGAR-log-file-data/2017/Qtr1/log20170120.zip',
    'http://www.sec.gov/dera/data/Public-EDGAR-log-file-data/2017/Qtr1/log20170121.zip',
    'http://www.sec.gov/dera/data/Public-EDGAR-log-file-data/2017/Qtr1/log20170122.zip',
    'http://www.sec.gov/dera/data/Public-EDGAR-log-file-data/2017/Qtr1/log20170123.zip',
    'http://www.sec.gov/dera/data/Public-EDGAR-log-file-data/2017/Qtr1/log20170124.zip',
    'http://www.sec.gov/dera/data/Public-EDGAR-log-file-data/2017/Qtr1/log20170125.zip',
    'http://www.sec.gov/dera/data/Public-EDGAR-log-file-data/2017/Qtr1/log20170126.zip',
    'http://www.sec.gov/dera/data/Public-EDGAR-log-file-data/2017/Qtr1/log20170127.zip',
    'http://www.sec.gov/dera/data/Public-EDGAR-log-file-data/2017/Qtr1/log20170128.zip',
    'http://www.sec.gov/dera/data/Public-EDGAR-log-file-data/2017/Qtr1/log20170129.zip',
    'http://www.sec.gov/dera/data/Public-EDGAR-log-file-data/2017/Qtr1/log20170130.zip',
    'http://www.sec.gov/dera/data/Public-EDGAR-log-file-data/2017/Qtr1/log20170131.zip',
]

if __name__ == '__main__':
    for i, url in enumerate(url_list_january_2017):
        download_and_convert_file(url)
    print(generate_comma_separated_filename_string())
