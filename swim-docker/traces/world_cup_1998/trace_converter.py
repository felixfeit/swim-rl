import csv
from datetime import datetime
from pathlib import Path


def write_day_increments_to_file(time_increments, file_name):
    with open(file_name, 'w') as f:
        for increment in time_increments:
            f.write(f'{increment}\n')


def convert_world_cup_file_to_traces(file='world_cup_1998_invocation_count.csv'):
    time_increments = []
    with open(file) as csvfile:
        csv_reader = csv.reader(csvfile, delimiter=',')
        next(csv_reader)
        first_row = next(csv_reader)
        time_increments.extend([60 / int(first_row[1])] * int(first_row[1]))
        last_time = datetime.strptime(first_row[0], '%Y-%m-%d %H:%M:%S')
        for row in csv_reader:
            new_time = datetime.strptime(row[0], '%Y-%m-%d %H:%M:%S')
            if new_time.day != last_time.day:
                write_day_increments_to_file(
                    time_increments,
                    last_time.strftime('world_cup_%Y-%m-%d.delta')
                )
                time_increments = []

            requests_per_minute = int(row[1])
            # interpolate over the minute
            time_increments.extend([60 / requests_per_minute] * requests_per_minute)
            last_time = new_time
        
        write_day_increments_to_file(
            time_increments,
            new_time.strftime('world_cup_%Y-%m-%d.delta')
        )


def generate_comma_separated_filename_string(prefix='traces/world_cup_1998/'):
    file_names = sorted([prefix + str(x) for x in list(Path('.').glob('*.delta')) if x.is_file()])
    return '"' + '", "'.join(file_names) + '"'


if __name__ == '__main__':
    convert_world_cup_file_to_traces() 
    print(generate_comma_separated_filename_string())
