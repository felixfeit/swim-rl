## Setup
### Prerequisites
1. Install `Docker`, `Git` and `Python 3`
2. Clone this repo

### Download and Prepare Traces
1. `cd` to root of the repo
2. `cd swim-docker/traces/edgar_2017`
3. `python3 trace_downloader.py` (this consumes ~4.5GB disk storage and runs ~5h)
4. `cd` to root of the repo
5. `cd swim-docker/traces/world_cup_1998`
6. `python3 trace_converter.py` (this consumes ~30GB disk storage and runs ~1h)

### Build the Docker Container
1. `cd` to root of the repo
2. `docker build -t swim ./swim-docker`

### Install the Gym Swim Adapter as a Pip Package
1. Optional: activate your virtual environment (`venv`) if you use one
2. `cd` to root of the repo
3. `pip install -e gym-swim-adapter`

### Use the Environment
1. Start `Docker`
2. Import using `from gym_swim_adapter.envs import SwimDiscreteActionsEnv`
3. Instantiate and call `reset()` **once**
4. On each time step call `step(action)` (behaves like a normal continuing gym enironment)
5. Whenever you need a new instance of the environment call `reset()`

## Configuration
### Simulation
1. Adapt the configuration in `swim-docker/simulations/swim_sa/swim_sa.ini`
2. `cd` to root of the repo
3. `docker build -t swim ./swim-docker`

### Adapter
The following arguments can be passed to the constructor of `SwimDiscreteActionsEnv`:
| Parameter Name             | Default Value          | Value Type      | Description                                                                                                  |
|----------------------------|------------------------|-----------------|--------------------------------------------------------------------------------------------------------------|
| `observation_variables`    | all possible observation variables (see below) | list of strings | The variables to include in the observation. Refer to Section "All Possible Observation Variables" for possible choices. **Note:** The observation is returned in the exact same order the variable names are passed to `observation_variables`.  |
| `docker_image_name`        | `'swim'`               | string          | The name of the docker image. Use this to distinguish images that have different simulation configurations.  |
| `store_simulation_results` | `True`                 | bool            | Whether the results of the simulation should be stored on disk (note that this occupies some disk space).    |
| `simulation_results_dir`   | `'simulation_results'` | string          | Where to store the simulation results. Only takes effect if `store_simulation_results` is set to `True`.       |
| `docker_host`              | `'localhost'`          | string          | Which host to connect to. This can be useful when running the adapter inside a docker-compose setup.         |
| `seed`                     | random integer         | int             | The initial RNG seed used for pseudo-random trace selection.                                                 |
| `round_robin_trace_selection` | `False`             | bool            | If set to `True` the traces for simulation are selected in a circular way. If set to `False` traces are selected in random order. Useful for evaluation and comparison of runs (to ensure a fairness). |
| `verbose`                  | `False`                | bool            | Whether to print certain informative logs to `stdout` or not.                                                |
| `moving_average_past_steps` | `20`                  | int             | How many past steps the moving averages (`request_arrival_moving_mean` and `request_arrival_moving_variance`) should include. |
| `container_name`           | `'auto_generated_swim_simulation_removed_after_run'` | string | The name of the docker container that is temporarily launched. Change this parameter if you want to run multiple instances of the environment in parallel. |
| `exposed_port`             | `60396`                | int             | The port that the launched docker container exposes. Change this parameter if the default port is already in use or you want to run multiple instances of the environment in parallel. |

#### All Possible Observation Variables
```
possible_observation_variables = [
    'basic_response_time',
    'opt_response_time',
    'basic_throughput',
    'opt_throughput',
    'avg_response_time',
    'utilization',
    'current_dimmer',
    'active_servers',
    'max_servers',
    'is_booting',
    'boot_remaining',
    'request_arrival_mean',
    'request_arrival_moving_mean',  # to fullfill the Markov property
    'request_arrival_variance',
    'request_arrival_moving_variance',  # to fullfill the Markov property
    'low_fidelity_service_time',
    'low_fidelity_service_time_variance',
    'service_time',
    'service_time_variance',
    'server_threads',
]
```
